*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary
Library    Collections

*** Variables ***
${baseUrl}          http://thetestingworldapi.com/


*** Test Cases ***
Post Request
    create session     AddData     ${baseUrl}
    &{body}=   create dictionary    first_name=Testing  middle_name=Arief  last_name=Marble  date_of_birth=01/04/1995  
    &{header}=  create dictionary    Content-Type=application/json
    
    ${response} =    post request    AddData    /api/studentsDetails   data=${body}   headers=${header}
    ${code}=   convert to string   ${response.status_code}
    should be equal   ${code}  201
    log to console     ${response.status_code}
    # log to console     ${response.content}