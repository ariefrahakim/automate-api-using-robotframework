*** Settings ***
Library    RequestsLibrary

*** Variables ***
${baseUrl}          http://thetestingworldapi.com/


*** Test Cases ***
Get Request
    create session     Get_Student_Details     ${baseUrl}
    ${response} =    get request    Get_Student_Details    /api/studentsDetails
    log to console     ${response.status_code}
    # log to console     ${response.content}
    ${code}=   convert to string   ${response.status_code}
    should be equal   ${code}  200