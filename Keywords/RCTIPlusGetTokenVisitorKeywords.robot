*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary
Resource   ../TestCases/RCTIPlusGetTokenVisitor.robot

*** Variables ***
${baseUrl}     https://rc-api.rctiplus.com
${path}       
${token_visitor}  

*** Keywords ***
Get Token Visitor
    create session     Get_Token_Visitor     ${baseUrl}     /api/v1/visitor?platform=android
    ${response} =    get request    Get_Token_Visitor    
    
