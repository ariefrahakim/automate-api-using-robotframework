*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary
Library    String

*** Variables ***
${baseUrl}          https://rc-api.rctiplus.com

*** Test Cases ***
Get News Category RCTI Plus

# Get token visitor

    create session     Get_Token_Visitor     ${baseUrl}     verify=True
    ${response} =    post request    Get_Token_Visitor    /api/v1/visitor?platform=android
    log to console     Response API Visitor
    log to console     ${response.content}
    
    # validation
    ${tokens}       get value from json      ${response.json()}       $.data.access_token
    ${tok}=      Get Substring   s${tokens}     3   -2
    log to console     tokenVisitor --------- ${tok}

    ${access_tokens}        set variable    ${tok}
    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}  200

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success

# Post Login

    create session     PostLogin     ${baseUrl}     verify=True
    &{body}=   create dictionary    username=qa_test@mailinator.com      password=dikakoko      device_id=3463784       platform=android 
    &{header}=  create dictionary   Content-Type=application/json      Authorization=${access_tokens}
    
    ${response} =    post request    PostLogin    /api/v3/login   data=${body}   headers=${header}
    log to console     Response API Login----------
    log to console     ${response.status_code}
    # log to console     ${response.content}

    # validation
    ${tokenLogin}       get value from json      ${response.json()}       $.data.access_token
    ${tokLogin}=      Get Substring   s${tokenLogin}     3   -2

    ${login_tokens}        set variable    ${tokLogin}
    log to console     tokenLogin--------- ${login_tokens}

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}  200

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success

# Post Token News RCTI Plus

    create session     GetTokenNews     ${baseUrl}     verify=True
    &{body}=   create dictionary    hostToken=${login_tokens}      merchantName=RCTI+      platform=mweb 
    &{header}=  create dictionary   Content-Type=application/json      
    
    ${response} =    post request    PostLogin    /news/api/v1/token   data=${body}   headers=${header}
    log to console     Response API Login----------
    log to console     ${response.status_code}
    # log to console     ${response.content}

    # validation
    ${tokenNews}       get value from json      ${response.json()}       $.data.news_token
    ${tokNews}=      Get Substring   s${tokenNews}     3   -2
    ${news_tokens}        set variable    ${tokNews}
    log to console     tokenNews--------- ${news_tokens}

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}  200

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success
    
# Get news category

    create session     Get_News_Category     ${baseUrl}     verify=True
    &{header}=      create dictionary        Authorization=${news_tokens} 
    ${response} =    get request    Get_News_Category    /news/api/v1/category      headers=${header}
      
    log to console     Response API News Category
    # log to console     ${response.content}
    
    # validation

    ${NewsCategory}       get value from json      ${response.json()}       $.data[2].title
    ${status_code}=   convert to string   ${response.status_code}
    log to console     ${NewsCategory}
    should be equal   ${status_code}  400

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success