*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary

*** Variables ***
${baseUrl}          https://rc-api.rctiplus.com



*** Test Cases ***
Get Token Visitor

    create session     tokenVisitorCuy     ${baseUrl}     verify=True
    ${response} =    get request    tokenVisitorCuy    /api/v1/visitor?platform=mweb&device_id=3463784
    log to console     ${response.status_code}
    log to console     ${response.content}
    
    # validation
    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}  200

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success


