*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary
# Library    Collections


*** Variables ***
${baseUrl}          https://rc-api.rctiplus.com


*** Test Cases ***
Post Request
    create session     Login     ${baseUrl}
    &{body}=   create dictionary    username=ariefrahmanhakim0104@gmail.com      password=AriefMay@20      device_id=3463784       platform=android 
    &{header}=  create dictionary   Content-Type=application/json      Authorization=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2aWQiOjAsInRva2VuIjoiNzUyYjM2ODY4NTNkM2NjMSIsInBsIjoibXdlYiIsImRldmljZV9pZCI6IjJmMTMwNjk3LWI4YWYtNDEyOC1iYjc5LWUzZmM4ZTQ5MzdiYyJ9.Vh_eAAOnj5tvEezM0IRdpCuxsce-Ld0e8OHyfjgb1Po
    
    ${response} =    post request    Login    /api/v3/login   data=${body}   headers=${header}
    log to console     Response API Login
    log to console     ${response.status_code}
    log to console     ${response.content}

    # validation
    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}  200

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success