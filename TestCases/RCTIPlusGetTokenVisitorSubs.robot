*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary
Library    String

*** Variables ***
${baseUrl}          https://rc-api.rctiplus.com

*** Test Cases ***
Get Token Visitor RCTI Plus

    create session     Get_Token_Visitor     ${baseUrl}     verify=True
    ${response} =    get request    Get_Token_Visitor    /api/v1/visitor?platform=android
    log to console     Response API Visitor
    log to console     ${response.content}
    
    # validation
    ${tokens}       get value from json      ${response.json()}       $.data.access_token
    ${tok}=      Get Substring   s${tokens}     3   -2
    log to console     tokenVisitor --------- ${tok}

    ${access_tokens}        set variable    ${tok}
    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}  200

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success

# Post Login

    create session     PostLogin     ${baseUrl}     verify=True
    &{body}=   create dictionary    username=ariefrahmanhakim0104@gmail.com      password=AriefMay@20      device_id=3463784       platform=android 
    &{header}=  create dictionary   Content-Type=application/json      Authorization=${access_tokens}
    
    ${response} =    post request    PostLogin    /api/v3/login   data=${body}   headers=${header}
    log to console     Response API Login----------
    log to console     ${response.status_code}
    # log to console     ${response.content}

    # validation
    ${tokenLogin}       get value from json      ${response.json()}       $.data.access_token
    ${tokLogin}=      Get Substring   s${tokenLogin}     3   -2
    log to console     tokenLogin--------- ${tokLogin}

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}  200

    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success




